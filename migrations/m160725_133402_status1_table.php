<?php

use yii\db\Migration;

class m160725_133402_status1_table extends Migration
{
    public function up()
    {
			$this->createTable(
		'status1',
			[
				'id' => 'pk',
				'name' => 'string',
			]
		);

    }

    public function down()
    {
        $this->dropTable('status1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
