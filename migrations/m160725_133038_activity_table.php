<?php

use yii\db\Migration;

class m160725_133038_activity_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'activity',
			[
				'id' => 'pk',
				'title' => 'string',
				'categoryId' => 'int',
				'statusId' => 'int'
			]
		);
    }

    public function down()
    {
       $this->dropTable('activity');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
