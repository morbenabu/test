<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;	
	

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }
	
	public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
	public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        /*if ($this->isAttributeChanged('statusId'))
            $this->statusId = Yii::$app->security->
					generatePasswordHash($this->statusId);*/

        if ($this->isNewRecord)
		    $this->statusId = 2;

        return $return;
    }
	
}


